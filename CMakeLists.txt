# Set the minimum version of cmake required to build this project
cmake_minimum_required(VERSION 2.6)
project(i_v4l2_camera)

find_package(blc_channel)

find_package(blc_program)
find_package(blc_image)

add_definitions(${BL_DEFINITIONS} -Wall -Wextra)
include_directories(${BL_INCLUDE_DIRS})
add_executable(i_v4l2_camera src/i_v4l2_camera.cpp src/v4l2.cpp)
target_link_libraries(i_v4l2_camera ${BL_LIBRARIES} -lv4l2)







