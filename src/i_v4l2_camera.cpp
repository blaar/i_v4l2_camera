//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "blc_core.h"
#include "blc_program.h"
#include "blc_channel.h"
#include "blc_image.h"
#include <unistd.h> //getpid

#include "v4l2.h"

#include <string.h>
#include <limits.h>
enum status {STOP, PAUSE, RUN};

blc_channel output;
int status=RUN;

#define DEFAULT_DEVICE_NAME "/dev/video0"
#define DEFAULT_OUTPUT_NAME "/i_v4l2_camera<pid>"


int callback(blc_array *image, void *)
{
	int status;
   status=blc_command_loop_start();
   blc_image_convert(&output, image);
   blc_command_loop_end();
   return status;
}

int main(int argc, char **argv){
   char const *device_name, *output_name, *output_type_option, *output_format_option;
   blc_array output_def;

	blv4l2_device camera;
	uint32_t output_type, output_format;
	int width, height;

   blc_program_set_description("Acquire webcam images on Linux computer (with video4linux2).");
   blc_program_add_option(&output_format_option, 'f', "format", "Y800", "set ouput video format", NULL);
   blc_program_add_option(&output_name, 'o', "output", "blc_channel-out", "output channel", DEFAULT_OUTPUT_NAME);
   blc_program_add_option(&output_type_option, 't', "type", "UIN8|FL32", "set ouput data type", NULL);
   blc_program_add_parameter(&device_name, "device", 0, "device path name", DEFAULT_DEVICE_NAME);

   blc_program_init(&argc, &argv, blc_quit);

   camera.init(device_name);

   if(strcmp(output_name, DEFAULT_OUTPUT_NAME)==0) SYSTEM_ERROR_CHECK(asprintf((char**)&output_name, ":i_v4l2_camera%d", getpid()), -1, NULL);

   if (output_type_option) output_type=STRING_TO_UINT32(output_type_option);
   else output_type=camera.image.type;

   if (output_format_option) output_format=STRING_TO_UINT32(output_format_option);
   else output_format=camera.image.format;

   width=camera.image.dims[camera.image.dims_nb-2].length;
   height=camera.image.dims[camera.image.dims_nb-1].length;

   blc_image_def(&output_def, output_type, output_format, width,  height);
   output.create_or_open(output_name, BLC_CHANNEL_WRITE, output_type, output_format, output_def.dims_nb, output_def.dims);
   blc_array_destroy(&output_def);

   output.publish();

   /**We set the synchronization sem for the loop*/
   blc_loop_try_add_waiting_semaphore(output.sem_ack_data);
   blc_loop_try_add_posting_semaphore(output.sem_new_data);

   blc_command_loop_init(0);
   camera.run(callback, NULL);


   return EXIT_SUCCESS;
}
